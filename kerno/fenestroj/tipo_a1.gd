extends "res://kerno/wablono/estonteco/fenestroj.gd"


# закрытие окна 
func close():
	$VBox.set_visible(false)


func _on_Button_x_button_up():
	close()


func _on_Button_full_button_up():
	var full_screen = OS.window_size - Vector2(52, 52)
	$VBox.rect_size = full_screen
	$VBox.rect_global_position = Vector2(48, 48)
	$VBox/HBox_top/top_texture2/HBox_buttons/Button_full.set_visible(false)
	# развернуть меню на весь экран
	fenestro_supren()

func _on_Button__button_up():
	#свернуть меню
	pass # Replace with function body.


func _on_Button_fix_button_up():
	#закрепить
	pass # Replace with function body.

func _drag(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$VBox.rect_position += event.relative
	#перетаскивание меню и поднятие его над другими окнами


func _on_resize_(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$VBox.rect_size += event.relative
	#изменение размера окна меню
	
func _on_Button_min_button_up():
	$VBox.rect_size = $VBox.rect_min_size
	$VBox/HBox_top/top_texture2/HBox_buttons/Button_full.set_visible(true)
	# Сворачивает окно в минимально установленный размер
	fenestro_supren()



