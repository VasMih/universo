extends Control


func _on_VBox_gui_input(event) -> void:
	if event is InputEventMouseButton and Input.is_mouse_button_pressed(BUTTON_LEFT):
		fenestro_supren()


# supren - окно supren - вверх
# поднимаем окно над другими окнами, меняя очерёдность
func fenestro_supren():
	self.get_parent().move_child(self, self.get_parent().get_child_count())
#	self.get_parent().get_parent().move_child(self.get_parent(), self.get_parent().get_parent().get_child_count())


