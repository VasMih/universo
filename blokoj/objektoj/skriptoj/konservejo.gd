extends "res://kerno/fenestroj/tipo_d1.gd"


onready var table = get_node("VBox/body_texture/HSplit/VBoxContainer2/Table")
onready var bottom = get_node("VBox/body_texture/HSplit/VBoxContainer2/HBoxContainer2")


var id_transmeti #переставить, переложить
var konservejo_aktuala # текущая выделенная строка


func _ready():
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('error = ',err)


func _on_data():
	var i_data_server = 0
	var masivo_forigo = [] # массив индексов на удаление
	for on_data in Net.data_server:
		if int(on_data['id']) == id_transmeti:
			# запустили новый проект перемещения объектов
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
			id_transmeti = 0
		i_data_server += 1
	for forigo in masivo_forigo:
		Net.data_server.remove(forigo)


#plenigi formularon - заполнить форму
func plenigi_formularon():
	var tree = $VBox/body_texture/HSplit/VBoxContainer/Tree
	#сохраняем выбранную позицию
	var select = tree.get_next_selected(null)
	var konservejo = null
	if select:
		konservejo = select.get_metadata(0)
	tree.clear()
	clear_listo()
	tree.set_hide_root(true)
	var test = tree.create_item(null,-1)
	test.set_text(0,'скрытый')
	var tree_sxipo = tree.create_item()
	tree_sxipo.set_text(0, Global.direktebla_objekto[Global.realeco-2]['nomo']['enhavo'] + ' - '+ Global.direktebla_objekto[Global.realeco-2]['uuid'])
	tree_sxipo.set_metadata(0,{'stokejo':null,'modulo':Global.direktebla_objekto[Global.realeco-2]})
	# склады в разных модулях
	if Global.direktebla_objekto[Global.realeco-2].get('ligilo'):
		for modulo in Global.direktebla_objekto[Global.realeco-2]['ligilo']['edges']:
			if modulo['node']['ligilo'].get('stokejo'):
				for stokejo in modulo['node']['ligilo']['stokejo']:
					var tree_konservejo = tree.create_item(tree_sxipo)
					tree_konservejo.set_text(0, stokejo['nomo']['enhavo'])
					tree_konservejo.set_metadata(0,{'stokejo':stokejo,'modulo':modulo['node']['ligilo']}) 
	# в станции выводим склад станции и остальные наши корабли в станции
	if Global.fenestro_stacio:
		var hangaro = tree.create_item(null,-1)
		hangaro.set_text(0,'Ангар')
		for objekto in Global.objektoj:
				var temp = tree.create_item(hangaro)
				var str1 = objekto['nomo']['enhavo'] + ' - ' + objekto['uuid']
				for posedanto in objekto['posedanto']['edges']:
					if posedanto['node']['posedantoUzanto']:
						str1 += ' - '+ posedanto['node']['posedantoUzanto']['retnomo']
				temp.set_text(0,str1)
				temp.set_metadata(0,{'stokejo':null,'modulo':objekto})
				if objekto.get('stokejo'):
					for stokejo in objekto['stokejo']:
						var tree_konservejo = tree.create_item(temp)
						tree_konservejo.set_text(0, stokejo['nomo']['enhavo'])
						tree_konservejo.set_metadata(0,{'stokejo':stokejo,'modulo':objekto}) 
				if objekto.get('ligilo'):
					for modulo in objekto['ligilo']['edges']:
						if modulo['node']['ligilo'].get('stokejo'):
							for stokejo in modulo['node']['ligilo']['stokejo']:
								var tree_konservejo = tree.create_item(temp)
								tree_konservejo.set_text(0, stokejo['nomo']['enhavo'])
								tree_konservejo.set_metadata(0,{'stokejo':stokejo,'modulo':modulo['node']['ligilo']}) 
	#восстанавливаем выбранную позицию
	if konservejo:
		# выделенный ранее объект
		var root = tree.get_root()
		if root:
			var item = sercxo_tree(root, konservejo)
			if item:
				item.select(0)


# проходим по дереву (tree) в поисках того де объекта и возвращаем элемент дерева
func sercxo_tree(root, objekto):
	var item = root.get_children() # первый наследник
	while item:
		var tek_konservejo = item.get_metadata(0) # считываем следующий элемент
		if tek_konservejo and tek_konservejo.get('modulo') and tek_konservejo['modulo']:
			if tek_konservejo['modulo']['uuid'] == objekto['modulo']['uuid']:
				return item
		# проверяем вложенные элементы
		var sercxo = sercxo_tree(item,objekto)
		if sercxo:
			return sercxo
		# берём следующий элемент
		item = item.get_next()
	return null


# очищаем окно 
func clear_listo():
	table.clear()


# plenigi - заполнить
func plenigi_listo(listoj):
	if not listoj.get('stokejo'):# нет склада
		return
	var items = listoj['stokejo']['objekto']['edges']
	var modulo = listoj['modulo']
	clear_listo()
	var i = 1
	var volumeno = 0
	table.columns = 4
	table.set_hide_root(true)
	# заполняем заголовки столбцов
	table.set_column_title(0,'')
	table.set_column_title(1,'внутренний объём')
	table.set_column_title(2,'внешний объём')
	table.set_column_title(3,'объём хранения')
	table.set_column_titles_visible(true)
	table.create_item()
	var item
	
	# enteno - содержа́ние (одной субстанции в другой)
	for enteno in items:
		item = table.create_item()
		# сохраняес данные строки, что бы точно идентифицировать
		item.set_metadata(0,enteno.duplicate(true))
		item.set_text(0,str(i) + ')' +
			String(enteno['node']['nomo']['enhavo']))
		if enteno['node']['volumenoInterna']:
			item.set_text(1,String(enteno['node']['volumenoInterna']))
		else:
			item.set_text(1,'-')
		if enteno['node']['volumenoEkstera']:
			item.set_text(2,String(enteno['node']['volumenoEkstera']))
		else:
			item.set_text(2,'-')
		if enteno['node']['volumenoStokado']:
			item.set_text(3,String(enteno['node']['volumenoStokado']))
		else:
			item.set_text(3,'-')
		if enteno['node']['volumenoStokado']:
			volumeno += enteno['node']['volumenoStokado']
		i += 1
	if modulo.get('volumenoInterna') and modulo['volumenoInterna']:
		bottom.get_node('all').text = String(modulo['volumenoInterna'])
	else:
		bottom.get_node('all').text = '-'
	bottom.get_node('free').text = String(volumeno)


# проверяем соответствие характеристик объектов (можно ли положить объект в объект)
# interkonveneco - совмести́мость
# objekto_ekstera - внешний объект
# objekto_interna - внутренний объект
func interkonveneco_objekto(objekto_ekstera, objekto_interna):
#	сверяем характеристики
	# варианты ответов:
	#	0 - не подходит
	#	1 - полностью подходит
	#	2 - можно помещять (склад рассчитан на большее (меньше показателей у склада и все совпадают))
	var set_ekstera = [] # множество характеристик внешнего объекта
	var set_interna = [] # множество характеристик внутреннего объекта
	for ekstera in objekto_ekstera['stato']['posedanto']['karakterizado']['edges']:
		if ((ekstera['node']['lateroKarakterizado'] == 'TUTA') or
			(ekstera['node']['lateroKarakterizado'] == 'INTERNE')):
			# если общая структура или внутрь, то берём на проверку в массив
			if !(ekstera['node']['tipo']['objId'] in set_ekstera):
				set_ekstera.append(ekstera['node']['tipo']['objId'])
	for interna in objekto_interna['stato']['posedanto']['karakterizado']['edges']:
		if ((interna['node']['lateroKarakterizado'] == 'TUTA') or
			(interna['node']['lateroKarakterizado'] == 'EKSTERA')):
			# если общая структура или наружу, то берём на проверку в массив
			if !(interna['node']['tipo']['objId'] in set_interna):
				set_interna.append(interna['node']['tipo']['objId'])
	if len(set_interna)>len(set_ekstera):
#		print('=== характеристик внутреннего объекта больше внешнего')
		return 0
	set_ekstera.sort()
	set_interna.sort()
	var per = 0
	for i in set_interna:
		if i != set_ekstera[per]:
			return 0
		per += 1
	if set_interna.size()<set_ekstera.size():
		return 2
#	print('== полностью подходит = ')
	return 1


# выбор склада
func _on_Tree_cell_selected():
	var tree = $VBox/body_texture/HSplit/VBoxContainer/Tree
	var select = tree.get_next_selected(null)
	var konservejo = select.get_metadata(0)
	clear_listo()
	if !konservejo:
		return null
	plenigi_listo(konservejo)
	# enteno - содержа́ние (одной субстанции в другой)


# перемещаем на станционный склад выделенную позицию
# transmeti - переставить, переложить
# - какой(какие) объект(ы)
# - на какой склад
# склад (место назначение) - это объект задачи, а перемещаемые объекты являются владельцами задачи
func transmeti(uuid_objekto, uuid_konservejo, uuid_stokejo):
	var query = Title.get_node("CanvasLayer/UI/UI/Taskoj").QueryObject.new()
	var taskoj = [] # список задач
	taskoj.append({
		'uuid_celo':uuid_konservejo,
		'nomo':"перемещение",
		'priskribo':"Перемещение объекта между складами",
		'kategorio':11
	})
	id_transmeti = Net.get_current_query_id()
	Net.send_json(query.krei_projekto_taskoj_posedanto(
		"Перемещение объектов",
		"Перемещение объектов между складами",
		Net.kategorio_movado,
		Net.projekto_tipo_objekto,
		Net.statuso_laboranta,
		null,
		0,0,0,0,0,0,
		null,null,
		Net.tipo_posedanto,
		Net.statuso_posedanto,
		taskoj,
		uuid_objekto,
		Net.tipo_posedanto,
		Net.statuso_posedanto,
		uuid_stokejo
	))


# перемещаем на станционный склад выделенную позицию
func _on_toStacio_pressed():
	fenestro_supren()
	var select = table.get_selected()
	var datenoj = select.get_metadata(0)
	if datenoj:
	#есть выделенный элемент and есть данные в выделенном элементе
		var uuid_objekto = datenoj['node']['uuid']
		var konservejo_stacio = Title.get_node("CanvasLayer/UI/UI/Objektoj").sercxo_tipo(1, 29)
			# konservejo_stacio - объект - склад
		if konservejo_stacio:
			# находим место хранение на станции
			if len(konservejo_stacio['universoobjektostokejoSet']['edges'])>0:
				transmeti(uuid_objekto, konservejo_stacio['uuid'], konservejo_stacio['universoobjektostokejoSet']['edges'].front()['node']['uuid'])


func _on_Tree_focus_entered():
	fenestro_supren()


func _on_Table_focus_entered():
	fenestro_supren()


func _on_HBoxContainer2_focus_entered():
	fenestro_supren()

