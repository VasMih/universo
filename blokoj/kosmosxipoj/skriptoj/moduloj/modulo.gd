extends Spatial

# общий скрипт для всех модулей

var uuid = ""
var integreco = 100
var potenco
var objekto
#var stokejo = [] #склад


# изменить данные в objekto
func sxangxi_objekto(item):
	if item['objekto']['ligiloLigilo']['edges'].front()['node']['tipo']['objId'] != 3:
		# если не находится на складе
		for obj in objekto['ligilo']['edges']:
			#ищем ресурс в переменной
			if obj['node']['ligilo']['uuid'] == item['objekto']['uuid']:
				# вносим изменения по данному объекту
				obj['node']['ligilo']['integreco'] = item['objekto']['integreco']
				obj['node']['ligilo']['volumenoInterna'] = item['objekto']['volumenoInterna']
				obj['node']['ligilo']['volumenoEkstera'] = item['objekto']['volumenoEkstera']
				obj['node']['ligilo']['volumenoStokado'] = item['objekto']['volumenoStokado']
				# если объект находится на складе, то изменяем и в stokejo
	#			print('== stokejo == ',stokejo.front()['uuid'])
	#			if 
				# если управляемый объект, изменяем в direktebla_objekto
	else:
		# если объект на складе
		print('если объект на складе - нужно доработать')
#		на склад не управляемого корабля
		pass


# добавляем ресурс в objekto
func krei_objekto(item):
	# если нет такого объекта, то добавляем его
	objekto['ligilo']['edges'].push_back({
		'node': {
			'uuid': item['objekto']['ligiloLigilo']['edges'].front()['node']['uuid'],
			'tipo': item['objekto']['ligiloLigilo']['edges'].front()['node']['tipo'].duplicate(true),
			'ligilo': {
				'uuid': item['objekto']['uuid'],
				'integreco': item['objekto']['integreco'],
				'nomo': {'enhavo': item['objekto']['nomo']['enhavo']},
				'volumenoInterna': item['objekto']['volumenoInterna'],
				'volumenoEkstera': item['objekto']['volumenoEkstera'],
				'volumenoStokado': item['objekto']['volumenoStokado'],
				'resurso': {'objId': item['objekto']['resurso']['objId']}
			}
		}
	})
