extends "res://kerno/fenestroj/tipo_a1.gd"

const LOKALIZO = preload("res://blokoj/administri/wablono/Lokalizo.tscn") #предзагружаю сцену lokalizo
#onready var grid = get_node("VBox/body_texture/ScrollContainer/VBoxContainer/GridContainer")

var objekto # строка данных для редактирования 
var horizontalo = 0 # строка

# horizontalo - строка (матрицы)
func aldoni_horizontalo3(nomo, tipo, informoj):
	# tipo - тип данных
	# informoj - данные
	horizontalo += 1
	var aldoni = Label.new()
	aldoni.text = nomo
	var grid = get_node("VBox/body_texture/ScrollContainer/VBoxContainer/GridContainer")
	print('columns01 = ',grid.columns)
	grid.add_child(aldoni)
	# яче́йка  ĉelo.
	var cxelo
	if tipo == 1:
		cxelo = LineEdit.new()
		cxelo.text = informoj
		cxelo.set_h_size_flags(3)
		grid.add_child(cxelo)
	var temp = Label.new()
	grid.add_child(temp)


# horizontalo - строка (матрицы)
func aldoni_horizontalo2(nomo, tipo, informoj, nomo_node):
	# tipo - тип данных
	#  1 - LineEdit
	#  2 - LocalizoEdit
	# informoj - данные
	horizontalo += 1
	var aldoni = Label.new()
	aldoni.name = str(horizontalo) + '===label'
	aldoni.text = nomo
	var grid = get_node("VBox/body_texture/ScrollContainer/VBoxContainer/GridContainer")
	grid.add_child(aldoni)
	# яче́йка  ĉelo.
	var cxelo
	var lingvoj = Title.get_node("CanvasLayer/UI/UI/main_administri").lingvoj #доступ к списку языков
	if tipo == 1:
		cxelo = LineEdit.new()
		cxelo.name = nomo_node
		cxelo.text = informoj
		cxelo.set_h_size_flags(3)
		grid.add_child(cxelo)
	elif tipo == 2: # подключение и обрабодка через сцену Lokalizo
		cxelo = LOKALIZO.instance()
		cxelo.name = nomo_node
		cxelo.set_h_size_flags(3)
		grid.add_child(cxelo)
		cxelo.analizado_lingo(lingvoj) #обрабодка (подгрузка) списка языков
		cxelo.analizado_json(informoj) #обрабодка json строки параметра
		pass


#purigi - очистить
# очистить GridContainer
func purigi_grid():
	var grid = get_node("VBox/body_texture/ScrollContainer/VBoxContainer/GridContainer")
	for child in grid.get_children():
		child.free()




