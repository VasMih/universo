extends "res://kerno/fenestroj/tipo_d1.gd"


const realeco_screan = preload("res://blokoj/administri/bazo/realeco.tscn")
const QueryAdmini = preload("queries.gd")
# const item_konservejo = preload("res://blokoj/objektoj/scenoj/ITEM_konservejo.tscn")
const apliko_tipo_screan = preload("res://blokoj/administri/bazo/apliko_tipo.tscn")

onready var table = get_node("VBox/body_texture/HSplit/VBoxContainer2/Table")

# var konservejo_aktuala # текущая выделенная строка
var id_postuloj = [] #postuloj - требования
# структура:
# id - id запроса
# modulo - какой модуль запрашиваем для редактирования
var id_lingvo # запрос на языки
var lingvoj = [] # список языков

func _ready():
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('error = ',err)


func _on_data():
	var i_data_server = 0
	var masivo_forigo = [] # массив индексов на удаление
	for on_data in Net.data_server:
		var index_postuloj = Title.BinaraSercxoId(id_postuloj, int(on_data['id']))
		if index_postuloj > -1: # находится в списке запрашиваемого содержания
			if id_postuloj[index_postuloj]['modulo'] == 'UniversoRealeco':
				plenigi_formularon_realeco(on_data['payload']['data']['universoRealeco']['edges'])
			elif id_postuloj[index_postuloj]['modulo'] == 'UniversoAplikoTipo':
				plenigi_formularon_aplikato_tipo(on_data['payload']['data']['universoAplikoTipo']['edges'])
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		elif id_lingvo == int(on_data['id']):
			# пришел список языков
			lingvoj.append_array(on_data['payload']['data']['informilojLingvoj']['edges'])
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		i_data_server += 1
	for forigo in masivo_forigo:
		Net.data_server.remove(forigo)


#plenigi formularon - заполнить форму
func plenigi_formularon():
	var tree = $VBox/body_texture/HSplit/VBoxContainer/Tree
	tree.clear()
	clear_listo()
	tree.set_hide_root(true)
	var test = tree.create_item(null,-1)
	test.set_text(0,'скрытый')
	# aplikaĵo - приложение
	var tree_aplikajo = tree.create_item()
	tree_aplikajo.set_text(0, 'Базовые справочники')
	tree_aplikajo.set_metadata(0,'universo_bazo')
	var tree_modelo = tree.create_item(tree_aplikajo)
	tree_modelo.set_text(0, 'справочник виртуальных миров')
	tree_modelo.set_metadata(0,'UniversoRealeco')
	var tree_modelo1 = tree.create_item(tree_aplikajo)
	tree_modelo1.set_text(0, 'справочник типов приложений')
	tree_modelo1.set_metadata(0,'UniversoAplikoTipo')


func _on_b_aldoni_pressed():
	"""
	проверяем, какая модель выбрана
	
	запускаем требуюемую сцену
	"""
	fenestro_supren()
	var tree = $VBox/body_texture/HSplit/VBoxContainer/Tree
	var select = tree.get_next_selected(null)
	if not select:
		return
	var modulo = select.get_metadata(0)
	
	if modulo == 'UniversoRealeco':
		var realeco = realeco_screan.instance()
		realeco.krei_grid(null, self)
		Title.get_node("CanvasLayer/UI/UI").add_child(realeco)
	elif modulo == 'UniversoAplikoTipo':
		var apliko_tipo = apliko_tipo_screan.instance()
		apliko_tipo.krei_grid(null, self)
		Title.get_node("CanvasLayer/UI/UI").add_child(apliko_tipo)


func _on_b_shangxi_pressed():
	"""
	проверяем, какая модель выбрана
	
	строку модели передаём для редактирования
	"""
	fenestro_supren()
	var tree = $VBox/body_texture/HSplit/VBoxContainer/Tree
	var select_tree = tree.get_next_selected(null)
	if not select_tree:
		return
	var modulo = select_tree.get_metadata(0)
	var select_table = table.get_next_selected(null)
	if not select_table:
		print('не выбран элемент для редактирования')
		return
	var modulo_table = select_table.get_metadata(0)
	print('=== ',modulo_table)
	
	if modulo == 'UniversoRealeco':
		var realeco = realeco_screan.instance()
		realeco.krei_grid(modulo_table, self)
		Title.get_node("CanvasLayer/UI/UI").add_child(realeco)
	elif modulo == 'UniversoAplikoTipo':
		var apliko_tipo = apliko_tipo_screan.instance()
		apliko_tipo.krei_grid(modulo_table, self)
		Title.get_node("CanvasLayer/UI/UI").add_child(apliko_tipo)


# обработка сигнала согласие на удаления
func forigi_signal(resulto):
	Title.get_node("CanvasLayer/UI/UI/demando").disconnect("resulto", self, "forigi_signal")
	print('получили сигнал = ',resulto)
	if !resulto:
		return
	if Title.get_node("CanvasLayer/UI/UI/demando").form !='main_administri':
		return
	var id = Net.get_current_query_id()
	var modulo = Title.get_node("CanvasLayer/UI/UI/demando").data['modulo']
	var modulo_table = Title.get_node("CanvasLayer/UI/UI/demando").data['modulo_table']
	if modulo == 'UniversoRealeco':
		print('--- modulo_table = ',modulo_table)
		var query = QueryAdmini.new()
		Net.send_json(query.forigi_realeco(modulo_table['node']['uuid'], id))

		pass
	elif modulo == 'UniversoAplikoTipo':
		var query = QueryAdmini.new()
		Net.send_json(query.forigi_applico_tipo(modulo_table['node']['uuid'], id))
		pass


func _on_b_forigi_pressed(): # функция при нажатии кнопки "Удалить"
	"""
	описание функции
	"""
	fenestro_supren() # добавил по аналогии, относитья к управлению окнами
	var tree = $VBox/body_texture/HSplit/VBoxContainer/Tree
	var select_tree = tree.get_next_selected(null)
	if not select_tree:
		return
	var modulo = select_tree.get_metadata(0)
	var select_table = table.get_next_selected(null)
	
	if not select_table:
		return
	var modulo_table = select_table.get_metadata(0)
	
	
	
	var err = Title.get_node("CanvasLayer/UI/UI/demando").connect("resulto", self, "forigi_signal")
	if err:
		print('error = ',err)
	Title.get_node("CanvasLayer/UI/UI/demando").form = 'main_administri'
	Title.get_node("CanvasLayer/UI/UI/demando").data = {
		'modulo':modulo, 
		'modulo_table':modulo_table
	}
	Title.get_node("CanvasLayer/UI/UI/demando/grafitio").text = 'Вы действительно\nхотите удалить\nстроку?'
	Title.get_node("CanvasLayer/UI/UI/demando/tipo_c/HBox_buttons/menuo_name").text = 'Вопрос'
	Title.get_node("CanvasLayer/UI/UI/demando").popup_centered()
	


func _on_Tree_cell_selected():
	"""
	выбор склада
	"""
	var tree = $VBox/body_texture/HSplit/VBoxContainer/Tree
	var select = tree.get_next_selected(null)
	var modulo = select.get_metadata(0)
	clear_listo()
	if modulo == 'UniversoRealeco':
		queryUniversoRealeco()
	elif modulo == 'UniversoAplikoTipo': # выбор 
		queryUniversoAplikoTipo() 


# очищаем окно 
func clear_listo():
	table.clear()


func queryUniversoRealeco():
	"""
	запрашиваем данные UniversoRealeco
	"""
	var temp_id = Net.get_current_query_id()
	id_postuloj.append({
		'id': temp_id,
		'modulo': 'UniversoRealeco'
	})
	var query = QueryAdmini.new()
	Net.send_json(query.get_realeco(temp_id))

func queryUniversoAplikoTipo():
	"""
	запрашиваем данные UniversoAplikoTipo
	"""
	var temp_id = Net.get_current_query_id()
	id_postuloj.append({
		'id': temp_id,
		'modulo': 'UniversoAplikoTipo'
	})
	var query = QueryAdmini.new()
	Net.send_json(query.get_apliko_tipo(temp_id))


func queryLingvo():
	"""
	запрашиваем потдерживаемые языки
	"""
	id_lingvo = Net.get_current_query_id()
	var query = QueryAdmini.new()
	Net.send_json(query.get_lingvo(id_lingvo))


# заполняем окно данными модели реальности
func plenigi_formularon_realeco(on_data):
	clear_listo()
	table.columns = 4
	table.set_hide_root(true)
	# заполняем заголовки столбцов
	table.set_column_title(0,'ID')
	table.set_column_title(1,'КОД')
	table.set_column_title(2,'наименование')
	table.set_column_title(3,'описание')
	table.set_column_titles_visible(true)
	table.create_item()
	var item
	# enteno - содержа́ние (одной субстанции в другой)
	for enteno in on_data:
		item = table.create_item()
		# сохраняем данные строки, что бы точно идентифицировать
		item.set_metadata(0,enteno.duplicate(true))
		item.set_text(0,String(enteno['node']['objId']))
		item.set_text(1,String(enteno['node']['kodo']))
		item.set_text(2,String(enteno['node']['nomo']['enhavo']))
		item.set_text(3,String(enteno['node']['priskribo']['enhavo']))


# заполняем окно данными модели типов приложения
func plenigi_formularon_aplikato_tipo(on_data):
	clear_listo()
	table.columns = 3
	table.set_hide_root(true)
	# заполняем заголовки столбцов
	table.set_column_title(0,'ID')
	table.set_column_title(1,'наименование')
	table.set_column_title(2,'описание')
	table.set_column_titles_visible(true)
	table.create_item()
	var item
	# enteno - содержа́ние (одной субстанции в другой)
	for enteno in on_data:
		item = table.create_item()
		# сохраняес данные строки, что бы точно идентифицировать
		item.set_metadata(0,enteno.duplicate(true))
		item.set_text(0,String(enteno['node']['objId']))
		item.set_text(1,String(enteno['node']['nomo']['enhavo']))
		item.set_text(2,String(enteno['node']['priskribo']['enhavo']))

func _on_fenestro_supren():
	fenestro_supren()


# renovigi - обновить 
func renovigi_modulo(modulo):
	"""
	обновляем правое окно (окно выбранного модуля),
	если выбран изменяемый модуль
	"""
	var tree = $VBox/body_texture/HSplit/VBoxContainer/Tree
	var select = tree.get_next_selected(null)
	var tree_modulo = select.get_metadata(0)
	clear_listo()
	if (modulo == 'UniversoRealeco')and(modulo == tree_modulo):
		queryUniversoRealeco()
	elif (modulo == 'UniversoAplikoTipo')and(modulo == tree_modulo): # выбор 
		queryUniversoAplikoTipo() 

