extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "Задачи"


# количество объектов для загрузки
const count_objekto = "4"


func projekto_query(id, after):
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'query ($realecoId:Float, $GlobalId:Int) { '+
		'universoTaskojProjekto (realeco_Id:$realecoId, '+
		' universotaskojprojektoposedanto_PosedantoUzanto_SiriusoUzanto_Id: $GlobalId, '+
		" first:"+count_objekto+', after: "'+after+'" ' +
		" ) { pageInfo { hasNextPage endCursor } edges { node { "+
		' uuid nomo { enhavo } priskribo { enhavo }'+
		' } } } }',
		"variables": {"realecoId":Global.realeco,
			"GlobalId":Global.id
		} }})
	# if Global.logs:
	# 	print('===taskoj_projekto==',query)
	Net.add_net_sendij(id, 'projekto_query')
	return query


func taskoj_query(projektoUuid, id, after):
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'query ($realecoId:Float, $projektoUuid:UUID ) { '+
		'universoTaskojTasko (realeco_Id:$realecoId, '+
		' projekto_Uuid:$projektoUuid, '+
		" first:"+count_objekto+', after: "'+after+'" ' +
		" ) { pageInfo { hasNextPage endCursor } edges { node { "+
		' uuid nomo { enhavo } priskribo { enhavo } '+
		' } } } }',
		"variables": {"realecoId":Global.realeco,
		"projektoUuid":projektoUuid} }})
	# if Global.logs:
	# 	print('=== taskoj_query_ws ==',query)
	Net.add_net_sendij(id, 'taskoj_query')
	return query


# устанавливаем проект
func instalo_projekto(objektoUuid, kom_koordX, kom_koordY, kom_koordZ,
		fin_koordX, fin_koordY, fin_koordZ, id,
		tipoId = 2,	kategorio = 3,	statusoId = 2,	nomo = "Movado",
		priskribo = "Movado de objekto", 
		statusoPosedantoId = 1, 
		tipoPosedantoId = 1, posedantoObjektoUuid =null):
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.append(id)
	if !posedantoObjektoUuid:
		posedantoObjektoUuid = objektoUuid
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($tipoId:Int, $kategorio:[Int],'+
		' $nomo:String, $priskribo:String, $statusoId:Int, $kom_koordX:Float, '+
		' $kom_koordY:Float, $kom_koordZ:Float, $fin_koordX:Float, '+
		' $fin_koordY:Float, $fin_koordZ:Float, $tipoPosedantoId:Int,'+
		' $statusoPosedantoId:Int, $objektoUuid:String, '+
		' $posedantoObjektoUuid:String, $realecoId:Int ) '+
		'{ redaktuUniversoTaskojProjekto ( '+
		' tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, publikigo:true, komKoordinatoX:$kom_koordX,'+
		' komKoordinatoY:$kom_koordY, komKoordinatoZ:$kom_koordZ, finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ, posedantoTipoId:$tipoPosedantoId, '+
		' objektoUuid: $objektoUuid, posedantoStatusoId:$statusoPosedantoId ,'+
		' posedantoObjektoUuid: $posedantoObjektoUuid,' +
		' realecoId:$realecoId ) '+
		' { status message '+
		' universoProjekto { uuid } } }',
		'variables': {"tipoId":tipoId, "kategorio": kategorio, "nomo": nomo,
			"priskribo": priskribo, "statusoId": statusoId,
			"kom_koordX": kom_koordX, "kom_koordY": kom_koordY, "kom_koordZ": kom_koordZ, 
			"fin_koordX":fin_koordX, 
			"fin_koordY":fin_koordY, "fin_koordZ":fin_koordZ,
			"objektoUuid":objektoUuid, "statusoPosedantoId":statusoPosedantoId,
			"posedantoObjektoUuid":posedantoObjektoUuid,
			"tipoPosedantoId":tipoPosedantoId, "realecoId":Global.realeco} }})
	# if Global.logs:
	# 	print('===instalo_projekto===',query)
	Net.add_net_sendij(id, 'instalo_projekto')
	return query


# записываем список задач с их владельцами
func instalo_tasko_posedanto_koord(uuid, projekto_uuid, kom_koordX, kom_koordY, kom_koordZ, itineroj, id=0):
	# создаём список задач, создаём владельца проекта, устанавливаем координаты объекту
	var tipoId = 2
	var kategorio = []
	var statusoId = [2] # первая задача в работе
	var nomo = []
	var priskribo = []
	var objektoUuid = []
	var tipoPosedantoId = 1
	var statusoPosedantoId = 1
	var pozicio =[1]
	var fin_koordX = []
	var fin_koordY = []
	var fin_koordZ = []
	# параметры координат последующих задач
	var i = 0
	var komKoordinatoX=[]
	var komKoordinatoY=[]
	var komKoordinatoZ=[]
	for iti in itineroj:
		kategorio.append(itineroj[i]['kategorio'])
		nomo.append(itineroj[i]['nomo'])
		priskribo.append(itineroj[i]['priskribo'])
		if i==0:
			komKoordinatoX.append(kom_koordX)
			komKoordinatoY.append(kom_koordY)
			komKoordinatoZ.append(kom_koordZ)
		else:
			pozicio.append(i+1)
			komKoordinatoX.append(itineroj[i-1]['koordinatoX'])
			komKoordinatoY.append(itineroj[i-1]['koordinatoY'])
			komKoordinatoZ.append(itineroj[i-1]['koordinatoZ'])
			statusoId.append(1) # новая задача
		fin_koordX.append(itineroj[i]['koordinatoX'])
		fin_koordY.append(itineroj[i]['koordinatoY'])
		fin_koordZ.append(itineroj[i]['koordinatoZ'])
		if itineroj[i]['uuid_celo']:
			objektoUuid.append(itineroj[i]['uuid_celo'])
		else:
			objektoUuid.append(null)
		i += 1

	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.append(id)
	var query = JSON.print({
		'type': 'start',
		'id': '%s' % id,
		'payload':{ 'query': 'mutation ( ' +
		'$tipoId:Int, $kategorio:[[Int]], $nomo:[String], $priskribo:[String], $statusoId:[Int], $projekto_uuid: String,' +
		'$komKoordinatoX:[Float], $komKoordinatoY:[Float], $komKoordinatoZ:[Float], $tipoPosedantoId:Int,' +
		'$fin_koordX:[Float], $fin_koordY:[Float], $fin_koordZ:[Float], $pozicio:[Int], $statusoPosedantoId:Int,' +
		'$objektoUuid:[String], $realecoId:Int, $posedantoObjektoUuid:String)'+
		' { redaktuKreiUniversoTaskojTaskojPosedanto (projektoUuid: $projekto_uuid, tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, pozicio:$pozicio, komKoordinatoX:$komKoordinatoX,'+
		' komKoordinatoY:$komKoordinatoY, komKoordinatoZ:$komKoordinatoZ, finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ, posedantoStatusoId:$statusoPosedantoId,'+
		' posedantoTipoId:$tipoPosedantoId, objektoUuid:$objektoUuid, posedantoObjektoUuid:$posedantoObjektoUuid,'+
		' realecoId:$realecoId ) { status '+
		' message universoTaskoj { uuid } } }',
		'variables':  {"tipoId":tipoId, "kategorio": kategorio, "nomo": nomo, "priskribo": priskribo, 
		"statusoId": statusoId, "projekto_uuid": projekto_uuid, 
		"tipoPosedantoId":tipoPosedantoId,
		"statusoPosedantoId":statusoPosedantoId, "objektoUuid":objektoUuid, "pozicio":pozicio,
		"posedantoObjektoUuid":uuid,
		"realecoId":Global.realeco,
		"fin_koordX":fin_koordX, "fin_koordY":fin_koordY, "fin_koordZ":fin_koordZ, 
		"komKoordinatoX":komKoordinatoX, "komKoordinatoY":komKoordinatoY, "komKoordinatoZ":komKoordinatoZ
		}}})
	# if Global.logs:
	# 	print('===instalo_tasko_posedanto_koord===',query)
	Net.add_net_sendij(id, 'instalo_tasko_posedanto_koord')
	return query


# создаём проект с владельцем и к нему записываем список задач с их владельцами
func krei_projekto_taskoj_posedanto( 
		prj_nomo, prj_priskribo, prj_kategorio,
		prj_tipo_id, prj_statuso_id, prj_objekto_uuid,
		kom_koordX, kom_koordY, kom_koordZ, 
		fin_koordX, fin_koordY, fin_koordZ,
		prj_posedanto_objekto_uuid,
		prj_posedanto_organizo_uuid,
		prj_posedanto_tipo_id,
		prj_posedanto_statuso_id,
		taskoj,
		# задачи передаются массивом струткур. Структура:
		# 'uuid_celo'
		# 'nomo'
		# 'priskribo'
		# 'kategorio'
		objektoPosedanto,
		tipoPosedantoId,
		statusoPosedantoId,
		universoStokejoUuid = null,
		id=0):
	# создаём список задач, создаём владельца проекта, устанавливаем координаты объекту
	var tipoId = 2
	var kategorio = []
	var statusoId = [2] # первая задача в работе
	var nomo = []
	var priskribo = []
	var objektoUuid = []
	var pozicio =[1]
	var finKoordinatoX = []
	var finKoordinatoY= []
	var finKoordinatoZ = []
	# параметры координат последующих задач
	var i = 0
	var komKoordinatoX=[]
	var komKoordinatoY=[]
	var komKoordinatoZ=[]
	var tempo = ''
	for tasko in taskoj:
		kategorio.append(taskoj[i]['kategorio'])
		nomo.append(taskoj[i]['nomo'])
		priskribo.append(taskoj[i]['priskribo'])
		if i==0:
			if kom_koordX and kom_koordY and kom_koordZ:
				komKoordinatoX.append(kom_koordX)
				komKoordinatoY.append(kom_koordY)
				komKoordinatoZ.append(kom_koordZ)
			else:
				komKoordinatoX.append(null)
				komKoordinatoY.append(null)
				komKoordinatoZ.append(null)
		else:
			pozicio.append(i+1)
			if taskoj[i-1].get('koordinatoX') and taskoj[i-1].get('koordinatoY') and taskoj[i-1].get('koordinatoZ'):
				komKoordinatoX.append(taskoj[i-1]['koordinatoX'])
				komKoordinatoY.append(taskoj[i-1]['koordinatoY'])
				komKoordinatoZ.append(taskoj[i-1]['koordinatoZ'])
			else:
				komKoordinatoX.append(null)
				komKoordinatoY.append(null)
				komKoordinatoZ.append(null)
			statusoId.append(1) # новая задача
		if taskoj[i].get('koordinatoX') and taskoj[i].get('koordinatoY') and taskoj[i].get('koordinatoZ'):
			finKoordinatoX.append(taskoj[i]['koordinatoX'])
			finKoordinatoY.append(taskoj[i]['koordinatoY'])
			finKoordinatoZ.append(taskoj[i]['koordinatoZ'])
		else:
			finKoordinatoX.append(null)
			finKoordinatoY.append(null)
			finKoordinatoZ.append(null)
		if taskoj[i].get('uuid_celo'):
			objektoUuid.append(taskoj[i]['uuid_celo'])
		else:
			objektoUuid.append(null)
		i += 1
	# если этих параметров нет, то и не должно быть упоминание в запросе
	if prj_posedanto_objekto_uuid:
		tempo +=' prjPosedantoObjektoUuid:"'+prj_posedanto_objekto_uuid+'", '
	if prj_posedanto_organizo_uuid:
		tempo +=' prjPosedantoOrganizoUuid:"'+prj_posedanto_organizo_uuid+'", '
	if prj_objekto_uuid:
		tempo +=' prjObjektoUuid:"'+prj_objekto_uuid+'", '
	if universoStokejoUuid:
		tempo +=' universoStokejoUuid:"'+universoStokejoUuid+'", '
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.append(id)
	var query = JSON.print({
		'type': 'start',
		'id': '%s' % id,
		'payload':{ 'query': 'mutation ( $prj_nomo: String, $prj_priskribo: String, '+
		' $prj_kategorio: [Int], $prj_tipo_id:Int, $prj_statuso_id:Int, '+
		' $kom_koordX:Float, $kom_koordY:Float, $kom_koordZ:Float, '+
		' $fin_koordX:Float, $fin_koordY:Float, $fin_koordZ:Float, '+
		' $prj_posedanto_tipo_id:Int, $prj_posedanto_statuso_id:Int, '+
		' $prjPosedantoUzantoId:Int,' + 
		'$tipoId:Int, $kategorio:[[Int]], $nomo:[String], $priskribo:[String], $statusoId:[Int], ' +
		'$komKoordinatoX:[Float], $komKoordinatoY:[Float], $komKoordinatoZ:[Float],' +
		'$finKoordinatoX:[Float], $finKoordinatoY:[Float], $finKoordinatoZ:[Float], $pozicio:[Int],' +
		'$objektoUuid:[String], $realecoId:Int,'+
		'$statusoPosedantoId:[Int], $tipoPosedantoId:[Int], $posedantoObjektoUuid:[String])'+
		' { redaktuKreiUniversoTaskojProjektoTaskojPosedanto ( '+
		' prjNomo:$prj_nomo, prjPriskribo:$prj_priskribo, prjKategorio:$prj_kategorio, '+
		' prjTipoId:$prj_tipo_id, prjStatusoId:$prj_statuso_id, '+
		' prjKomKoordinatoX:$kom_koordX, prjKomKoordinatoY:$kom_koordY, prjKomKoordinatoZ:$kom_koordZ, '+
		' prjFinKoordinatoX:$fin_koordX, prjFinKoordinatoY:$fin_koordY, prjFinKoordinatoZ:$fin_koordZ, '+
		tempo +
		' prjPosedantoTipoId:$prj_posedanto_tipo_id, '+
		' prjPosedantoStatusoId:$prj_posedanto_statuso_id, '+
		' prjPosedantoUzantoId:$prjPosedantoUzantoId, '+
		' tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, pozicio:$pozicio, komKoordinatoX:$komKoordinatoX,'+
		' komKoordinatoY:$komKoordinatoY, komKoordinatoZ:$komKoordinatoZ, finKoordinatoX:$finKoordinatoX,'+
		' finKoordinatoY:$finKoordinatoY, finKoordinatoZ:$finKoordinatoZ, posedantoStatusoId:$statusoPosedantoId,'+
		' posedantoTipoId:$tipoPosedantoId, objektoUuid:$objektoUuid, posedantoObjektoUuid:$posedantoObjektoUuid,'+
		' realecoId:$realecoId ) { status '+
		' universoProjekto { uuid } ' +
		' message universoTaskoj { uuid } } }',
		'variables':  {"prj_nomo":prj_nomo, "prj_priskribo":prj_priskribo, "prj_kategorio":prj_kategorio,
		"prj_tipo_id":prj_tipo_id, "prj_statuso_id":prj_statuso_id, 
		"prj_posedanto_tipo_id":prj_posedanto_tipo_id,
		"prj_posedanto_statuso_id":prj_posedanto_statuso_id,
		"prjPosedantoUzantoId":Global.id,			
		"realecoId":Global.realeco,
		"tipoId":tipoId, "kategorio": kategorio, "nomo": nomo, "priskribo": priskribo, 
		"statusoId": statusoId, "objektoUuid":objektoUuid, "pozicio":pozicio,
		"tipoPosedantoId":tipoPosedantoId,
		"statusoPosedantoId":statusoPosedantoId, 
		"posedantoObjektoUuid":objektoPosedanto,
		"kom_koordX":kom_koordX, "kom_koordY":kom_koordY, "kom_koordZ":kom_koordZ, 
		"fin_koordX":fin_koordX, "fin_koordY":fin_koordY, "fin_koordZ":fin_koordZ,
		"finKoordinatoX":finKoordinatoX, "finKoordinatoY":finKoordinatoY, "finKoordinatoZ":finKoordinatoZ, 
		"komKoordinatoX":komKoordinatoX, "komKoordinatoY":komKoordinatoY, "komKoordinatoZ":komKoordinatoZ
		}}})
#	if Global.logs:
#		print('===krei_projekto_taskoj_posedanto===',query)
#		# сохраняем в файл
#		Global.saveFileLogs('krei_projekto_taskoj_posedanto')
#		Global.saveFileLogs(query)
	Net.add_net_sendij(id, 'krei_projekto_taskoj_posedanto')
	return query


# создаём задачу, изменяем финальные координаты проекту
func instalo_tasko_koord(posedantoObjektoUuid, projekto_uuid, kom_koordX, kom_koordY, kom_koordZ,
		fin_koordX, fin_koordY, fin_koordZ, id=0):
	var posedantoTipoId = 1
	var posedantoStatusoId = 1
	var tipoId = 2
	var kategorio = 3 # категория движения объектов в космосе
	var statusoId = 2
	var nomo = "Movado"
	var priskribo = "Movado de objekto"
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.append(id)
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($koordX:Float, $koordY:Float, $koordZ:Float, '+
		'$tipoId:Int, $kategorio:[Int], $nomo:String, $priskribo:String, $statusoId:Int, $projekto_uuid: UUID,'+
		'$fin_koordX:Float, $fin_koordY:Float, $fin_koordZ:Float, $projektoUuid: String, '+
		'$posedantoTipoId:Int, $posedantoStatusoId:Int, $posedantoObjektoUuid:String, $UzantoId:Int, '+
		'$realecoId:Int)'+
		'{ redaktuUniversoTaskojTasko (projektoUuid: $projektoUuid, tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, publikigo:true, komKoordinatoX:$koordX,'+
		' komKoordinatoY:$koordY, komKoordinatoZ:$koordZ, finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ, posedantoTipoId:$posedantoTipoId, '+
		' posedantoObjektoUuid:$posedantoObjektoUuid, posedantoStatusoId:$posedantoStatusoId, '+
		' posedantoUzantoSiriusoUzantoId: $UzantoId, ' +
		' realecoId:$realecoId) '+
		'{ status '+
		' message universoTaskoj { uuid } } '+
		'redaktuUniversoTaskojProjekto ( uuid:$projekto_uuid ,'+
		' finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ ) '+
		' { status message '+
		' universoProjekto { uuid } } }',
		'variables': {"koordX": kom_koordX, "koordY": kom_koordY, "koordZ": kom_koordZ,
		"tipoId":tipoId, "kategorio": kategorio, "nomo": nomo, "priskribo": priskribo, 
		"statusoId": statusoId, "projekto_uuid": projekto_uuid, "projektoUuid": projekto_uuid, 
		"fin_koordX":fin_koordX, "fin_koordY":fin_koordY, "fin_koordZ":fin_koordZ,
		"posedantoStatusoId":posedantoStatusoId, "posedantoTipoId":posedantoTipoId,
		"UzantoId":Global.id, 
		"posedantoObjektoUuid":posedantoObjektoUuid, "realecoId":Global.realeco } }})
	# if Global.logs:
	# 	print('===instalo_tasko_koord===',query)
	Net.add_net_sendij(id, 'instalo_tasko_koord')
	return query


