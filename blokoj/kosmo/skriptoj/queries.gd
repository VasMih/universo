extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "Космос"


# подписка на действия в кубе
func kubo_json(id=0):
	if !id:
		id = Net.get_current_query_id()
	var statusoIdIn = '"'+String(Net.statuso_nova) + ','+String(Net.statuso_laboranta) + ','+String(Net.status_pauzo)+'"'
	var statusoId = String(Net.statuso_laboranta)
	var tipoId = String(Net.tasko_tipo_objekto)
	var tipoLigilo = '"'+'1, 2'+'"'
	var q = Title.QueryObject.new()
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'subscription ($kuboj:[Int]!, $realeco:Int!)'+
		'{ universoObjektoEventoj (kuboj: $kuboj, realeco:$realeco) { evento '+
		' objekto { uuid koordinatoX koordinatoY koordinatoZ '+
		"  nomo { enhavo } priskribo { enhavo } "+
		"  integreco volumenoInterna volumenoEkstera volumenoStokado " +
		"  kubo{objId} " +
		"  posedantoId " +
		"  posedanto{edges{node{" +
		"   posedantoUzanto{ retnomo siriusoUzanto{ objId}}}}}" +
		"  stato{objId potenco integreco statoAntaua {integreco} "+
		"   statoSekva{integreco}}" +
		"  resurso{objId tipo{objId}}" +
		"  ligiloLigilo{edges{node{ uuid "+
		"   tipo {objId} " +
		"   posedantoStokejo { uuid nomo{enhavo} " +
		"    priskribo { enhavo } "+
		"    posedantoObjekto   { uuid } } "+
		"   posedanto{ kubo {objId} koordinatoX koordinatoY " +
		"    koordinatoZ uuid nomo {enhavo} }}}}" +
		q.query_universoobjektostokejoSet +
		q.qyery_stato +
		q.query_ligilo_starto + "(tipo_Id_In: "+tipoLigilo+")" +
		q.query_ligilo_mezo +
		q.query_ligilo_starto + "(tipo_Id_In: "+tipoLigilo+" )" +
		q.query_ligilo_mezo +
		q.query_ligilo_fino +
		q.query_ligilo_fino +
		"  projekto (statuso_Id: "+statusoId+", tipo_Id: "+tipoId+"){ "+
		"   edges { node { uuid statuso{objId} "+
		"   kategorio {edges {node {objId nomo{enhavo}}}}" +
		"   tasko (statuso_Id_In: "+statusoIdIn+"){ edges {node { "+
		"    uuid finKoordinatoX finKoordinatoY finKoordinatoZ "+
		"    objekto{uuid} " +
		"    nomo{enhavo} priskribo{enhavo} " +
		"    kategorio {edges {node {objId nomo{enhavo}}}}" +
		"    pozicio statuso {objId} } } } } } } "+
		'  rotaciaX rotaciaY rotaciaZ } '+
		' projekto {uuid statuso{objId} kategorio {edges {node {objId }}}} '+
		' tasko { uuid komKoordinatoX komKoordinatoY '+
		"  nomo{enhavo} priskribo{enhavo} " +
		"  objekto{uuid} " +
		'  posedanto{edges{node{ posedantoObjekto{uuid} }}}' +
		'  komKoordinatoZ finKoordinatoX finKoordinatoY '+
		'  finKoordinatoZ pozicio statuso{objId} kategorio { '+
		'  edges { node { objId } } }} '+
		' mastro {uuid nomo{enhavo}}' +
		' posedi {uuid nomo {enhavo}}' +
		'} }',
		'variables': {"kuboj": Global.kubo, "realeco": Global.realeco} }})
	# if Global.logs:
	# 	Global.saveFileLogs('=== kubo_json =query= ')
	# 	Global.saveFileLogs(query)
		# print('=== kubo_json =query= ',query)
	Net.add_net_sendij(id, 'kubo_json')
	return query


