extends Spatial


const QueryObject = preload("queries.gd")

const sxipo = preload("res://blokoj/kosmosxipoj/scenoj/sxipo_fremdulo.tscn")
const sxipo_modulo = preload("res://blokoj/kosmosxipoj/skriptoj/moduloj/sxipo.gd")
const base_ship = preload("res://blokoj/kosmosxipoj/scenoj/base_ship.tscn")
const debris = preload("res://blokoj/kosmosxipoj/scenoj/debris.tscn")
const espero = preload("res://blokoj/kosmostacioj/espero/espero_ekster.tscn")
const asteroido = preload("res://blokoj/asteroidoj/asteroido.tscn")
const ast_1 = preload("res://blokoj/asteroidoj/ast_1.tscn")
const ast_1_cr = preload("res://blokoj/asteroidoj/ast_1_cr.tscn")
const ast_2 = preload("res://blokoj/asteroidoj/ast_2.tscn")
const ast_2_cr = preload("res://blokoj/asteroidoj/ast_2_cr.tscn")
const ast_3 = preload("res://blokoj/asteroidoj/ast_3.tscn")
const ast_3_cr = preload("res://blokoj/asteroidoj/ast_3_cr.tscn")
const ast_4 = preload("res://blokoj/asteroidoj/ast_4.tscn")
const ast_4_cr = preload("res://blokoj/asteroidoj/ast_4_cr.tscn")
const ast_5 = preload("res://blokoj/asteroidoj/ast_5.tscn")
const ast_5_cr = preload("res://blokoj/asteroidoj/ast_5_cr.tscn")
const ast_shards_0 = preload("res://blokoj/asteroidoj/ast_shards_0.tscn")
const ast_shards_1 = preload("res://blokoj/asteroidoj/ast_shards_1.tscn")
const ast_shards_2 = preload("res://blokoj/asteroidoj/ast_shards_2.tscn")
const ast_ice_1 = preload("res://blokoj/asteroidoj/ast_ice_1.tscn")
const ast_ice_1_cr = preload("res://blokoj/asteroidoj/ast_ice_1_cr.tscn")
const ast_ice_2 = preload("res://blokoj/asteroidoj/ast_ice_2.tscn")
const ast_ice_2_cr = preload("res://blokoj/asteroidoj/ast_ice_2_cr.tscn")
const ast_ice_3 = preload("res://blokoj/asteroidoj/ast_ice_3.tscn")
const ast_ice_3_cr = preload("res://blokoj/asteroidoj/ast_ice_3_cr.tscn")
const ast_ice_4 = preload("res://blokoj/asteroidoj/ast_ice_4.tscn")
const ast_ice_4_cr = preload("res://blokoj/asteroidoj/ast_ice_4_cr.tscn")
const ast_ice_5 = preload("res://blokoj/asteroidoj/ast_ice_5.tscn")
const ast_ice_5_cr = preload("res://blokoj/asteroidoj/ast_ice_5_cr.tscn")
const ast_ice_shards_0 = preload("res://blokoj/asteroidoj/ast_ice_shards_0.tscn")
const ast_ice_shards_1 = preload("res://blokoj/asteroidoj/ast_ice_shards_1.tscn")
const ast_ice_shards_2 = preload("res://blokoj/asteroidoj/ast_ice_shards_2.tscn")

const scene_objekto = preload("res://blokoj/objektoj/scenoj/objekto.tscn")

const AnalizoProjekto = preload("res://blokoj/kosmosxipoj/skriptoj/itineroj.gd")


# warning-ignore:unused_signal
signal load_objektoj

# список id запросов, отправленных на сервер
var id_projekto_direkt_del = [] # список проектов на удаление
var subscription_id # id сообщения, по какому объекту, в каком проекте, какая задача изменена (подписка universoObjektoEventoj)

var node_objekto #нода объекта прикручена к сцене Title и должна быть удалена при закрытии

func _ready():
	if Global.logs:
		print('запуск res://blokoj/kosmo/scenoj/space.tscn')
	$Control.camera = $camera
	# очищаем список задач
	Global.fenestro_itinero.malplenigi_itinero()
	Global.fenestro_itinero.projekto_itineroj_uuid = ''

	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('error = ',err)

	Global.fenestro_kosmo = self
	# считываем размер экрана и задаём затемнение на весь экран
	$ui/loading.margin_right = get_node("/root").get_viewport().size.x
	$ui/loading.margin_bottom = get_node("/root").get_viewport().size.y
	var ship = null
	if Global.server:
		ship = base_ship.instance()
		$camera.translation=Vector3(0,0,0)
	else:
		# создаём свой корабль
		ship = create_ship(Global.direktebla_objekto[Global.realeco-2])
		#если корабль игрока, то брать данные из direktebla_objekto
		$ui_armilo.sxipo = ship
		$camera.translation=Vector3(
			Global.direktebla_objekto[Global.realeco-2]['koordinatoX'],
			Global.direktebla_objekto[Global.realeco-2]['koordinatoY'], 
			Global.direktebla_objekto[Global.realeco-2]['koordinatoZ']+22
		)

	add_child(ship,true)

	$camera.point_of_interest = ship

	for i in get_children():
		if has_signal_custom(i,"new_way_point"):
			i.connect("new_way_point",self,"set_way_point")
	
	node_objekto = scene_objekto.instance()
	Title.ui.add_child(node_objekto,true)


func _on_data():
	var i_data_server = 0
	var masivo_forigo = [] # массив индексов на удаление
	for on_data in Net.data_server:
		var index = id_projekto_direkt_del.find(int(on_data['id']))
		if index > -1: # находится в списке удаляемых проектов
			# удаляем из списка
			var idx_prj = 0 #индекс массива для удаления
			for prj in Global.direktebla_objekto[Global.realeco-2]['projekto']['edges']:
				if prj['node']['uuid']==on_data['payload']['data']['redaktuUniversoTaskojProjekto']['universoProjekto']['uuid']:
					Global.direktebla_objekto[Global.realeco-2]['projekto']['edges'].remove(idx_prj)
					break
				idx_prj += 1
			id_projekto_direkt_del.remove(index)
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		elif int(on_data['id']) == subscription_id:
#			Global.saveFileLogs('пришла подписка')
#			Global.saveFileLogs(on_data)
			if on_data['payload']['data']['universoObjektoEventoj']['evento']=='kubo_tasko':
#				if Global.logs:
#					print('===пришла подписка на задачу ==kubo_tasko==')
#					Global.saveFileLogs('пришла подписка на задачу kubo_tasko on_data')
#					Global.saveFileLogs(on_data)
				# пришло сообщение, по какому объекту, в каком проекте, какая задача изменена
				sxangxi_tasko(on_data['payload']['data'])
			elif on_data['payload']['data']['universoObjektoEventoj']['evento']=='kubo_objekto':
				# изменение по объекту
#				if Global.logs:
#					print('=== редактируем объект по подписке ===',on_data['payload']['data']['universoObjektoEventoj']['objekto']['uuid'])
#					Global.saveFileLogs('пришла подписка на объект - on_data')
#					Global.saveFileLogs(on_data)
				sxangxi_objekto(on_data['payload']['data'])
			else:
				print('=== пришли альтернативные данные === ')
				Global.saveFileLogs('=== пришли альтернативные данные === ')
				Global.saveFileLogs(on_data)
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		i_data_server += 1
	for forigo in masivo_forigo:
		Net.data_server.remove(forigo)


# проверка, входит ли uuid в данный объект
#проверка по нисходящий (по всем детям-объектам)
# возвращается ссылка на модуль
func sercxado_uuid(uuid, objekt):
	var result = null
	if objekt.get('uuid'):
		if uuid == objekt.uuid:
			return objekt
	for ch in objekt.get_children():
		result = sercxado_uuid(uuid, ch)
		if result:
			return result
	return result


# поиск объекта в космосе по uuid
func sercxo_objekto_uuid(uuid):
	for ch in get_children():
		if ch.is_in_group('create'):
			if (ch.uuid == uuid):
				return ch
	return null


# поиск объекта, их частей и вложенных по uuid
func sercxo_objektoj_uuid(uuid, uuid_mastro, uuid_posedi):
	for ch in get_children():
		if ch.is_in_group('create'):
			if (ch.uuid == uuid_mastro) or (ch.uuid == uuid_posedi) or\
					(ch.uuid == uuid):
				return ch
	return null


# меняем задачу объекту
func sxangxi_tasko(on_data):
	# пометка о нахождении нужного объекта
#	var objekto = false
	# если эти изменения не по задаче
	if not on_data['universoObjektoEventoj']['tasko']:
		return
	var uuid = on_data['universoObjektoEventoj']['tasko']['posedanto']['edges'].front()['node']['posedantoObjekto']['uuid']
	for ch in get_children(): # проходим по всем созданным объектам в поисках нужного
		if sercxado_uuid(uuid,ch):
#			отправляю астероиду
#			if on_data['universoObjektoEventoj']['objekto']['uuid']==ch.uuid:
				# если это задача категории движения
			if on_data['universoObjektoEventoj']['projekto']['kategorio']['edges'].front()['node']['objId']==Net.kategorio_movado:
					# изменение маршрута движения
				if ch.is_in_group('create'):
					ch.sxangxi_itinero(on_data['universoObjektoEventoj']['projekto'],
						on_data['universoObjektoEventoj']['tasko'])
				# если проект стрельбы
			elif on_data['universoObjektoEventoj']['projekto']['kategorio']['edges'].front()['node']['objId']==Net.projekto_kategorio_pafado:
				ch.pafo(on_data['universoObjektoEventoj']['projekto'],
					on_data['universoObjektoEventoj']['tasko'])
				if on_data['universoObjektoEventoj']['tasko']['kategorio']['edges'].front()['node']['objId']==Net.tasko_kategorio_celilo:
					# сообщаем кораблю игрока о прицеливании в него
					pass
#			objekto = true
#	if !objekto: # если объект не найден, то нужно его добавить (если он не часть своего корабля)
#		if sercxado_uuid(uuid,$ship):
#			print('часть управляемого корабля uuid=',uuid)
#		else: # появился новый объект в космосе. Например - корабль вышел из дока станции
#			print('===найден новый объект!!! uuid=',uuid) #,' ===on_data=',on_data)


# изменяем характеристики объекта
func sxangxi_objekto(on_data):
	var uuid = on_data['universoObjektoEventoj']['objekto']['uuid']
	var uuid_mastro
	var uuid_posedi
	if on_data['universoObjektoEventoj']['mastro']:
		uuid_mastro = on_data['universoObjektoEventoj']['mastro']['uuid']
	if on_data['universoObjektoEventoj']['posedi']:
		uuid_posedi = on_data['universoObjektoEventoj']['posedi']['uuid']
	var objekto = sercxo_objektoj_uuid(uuid, uuid_mastro, uuid_posedi)
	if objekto: # объект в космосе найден
		# если объект, которым управляем
		# передавать и два внешних
		sxangxi_instance(objekto, on_data['universoObjektoEventoj'])
		# если открыты данные по объекту - обновляем
		if node_objekto.objekto and node_objekto.objekto.uuid == objekto.uuid:
			node_objekto.FillItemList()
	else: # ранее объекта не было - добавляем его
		# добавляем объект в Global.objektoj
		Title.get_node("CanvasLayer/UI/UI/Objektoj").aldoni_objekto(on_data['universoObjektoEventoj']['objekto'])
		# добавляем объект в космос
		analizo_item(on_data['universoObjektoEventoj']['objekto'])
	#обновляем список объектов в космосе
	Title.get_node("CanvasLayer/UI/UI/Objektoj")._reload_objekto()


# подписка на действие в кубе нахождения
func subscribtion_kubo():
	var q = QueryObject.new()
	subscription_id = Net.get_current_query_id()
	if Global.logs:
		print('===subscription_id === ',subscription_id)
	Net.send_json(q.kubo_json(subscription_id))


# отписка от действий в кубе
# nuligo - отмена (эсперанто)
func nuligo_subscribtion_kubo():
	var query = JSON.print({
		'type': 'stop',
		'id': '%s' % subscription_id})
	if Global.logs:
		print('=== nuligo_subscribtion_kubo =query= ',query)
	Net.send_json(query)
	subscription_id = null


func has_signal_custom(node, sgnl) ->bool:
	if node == null:
		return false
	for i in node.get_signal_list():
		if i.name == sgnl:
			return true
	return false


# функция создания управляемого корабля
func create_ship(objecto):
	var ship = null

	if (objecto['resurso']['objId'] == 3)or(objecto['resurso']['objId'] == 12):# это корабль "Vostok U2" "Базовый космический корабль"
		ship = base_ship.instance()
		var sh = sxipo_modulo.new()
		if objecto['stato']['objId']==5:# полностью разрушенный корабль
			ship.add_child(debris.instance())
		else:
			sh.create_sxipo(ship, objecto)
	if not ship: # проверка, если такого корабля нет в программе
		return null
	ship.translation=Vector3(objecto['koordinatoX'],
		objecto['koordinatoY'], objecto['koordinatoZ'])
	if objecto['rotaciaX']:
		ship.rotation=Vector3(objecto['rotaciaX'],
			objecto['rotaciaY'], objecto['rotaciaZ'])
	ship.visible=true
	ship.uuid=objecto['uuid']
	ship.objekto = objecto #.duplicate(true)
	ship.add_to_group('create')
	$camera.set_privot(ship)
	return ship


# добавляем объект в космос с настройкой местоположения
func add_objekto(objekto,item):
	objekto.translation=Vector3(item['koordinatoX'],
		item['koordinatoY'], item['koordinatoZ'])
	objekto.uuid = item['uuid']
	objekto.objekto = item.duplicate(true)
	objekto.rotation=Vector3(item['rotaciaX'],
		item['rotaciaY'], item['rotaciaZ'])
	add_child(objekto)
	objekto.add_to_group('create')


# создание астероида согласно его состояния
func krei_asteroido(asteroido,stato,tipo):
	if tipo == 21:
		if stato==1:
			asteroido.add_child(ast_1.instance())
		elif stato==2:
			asteroido.add_child(ast_1_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_shards_1.instance())
		else:
			asteroido.add_child(ast_shards_2.instance())
	elif tipo == 7:
		if stato==1:
			asteroido.add_child(ast_2.instance())
		elif stato==2:
			asteroido.add_child(ast_2_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_shards_1.instance())
		else:
			asteroido.add_child(ast_shards_2.instance())
	elif tipo == 22:
		if stato==1:
			asteroido.add_child(ast_3.instance())
		elif stato==2:
			asteroido.add_child(ast_3_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_shards_1.instance())
		else:
			asteroido.add_child(ast_shards_2.instance())
	elif tipo == 23:
		if stato==1:
			asteroido.add_child(ast_4.instance())
		elif stato==2:
			asteroido.add_child(ast_4_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_shards_1.instance())
		else:
			asteroido.add_child(ast_shards_2.instance())
	elif tipo == 24:
		if stato==1:
			asteroido.add_child(ast_5.instance())
		elif stato==2:
			asteroido.add_child(ast_5_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_shards_1.instance())
		else:
			asteroido.add_child(ast_shards_2.instance())
	elif tipo == 20:
		if stato==1:
			asteroido.add_child(ast_ice_1.instance())
		elif stato==2:
			asteroido.add_child(ast_ice_1_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_ice_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_ice_shards_1.instance())
		else:
			asteroido.add_child(ast_ice_shards_2.instance())
	elif tipo == 25:
		if stato==1:
			asteroido.add_child(ast_ice_2.instance())
		elif stato==2:
			asteroido.add_child(ast_ice_2_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_ice_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_ice_shards_1.instance())
		else:
			asteroido.add_child(ast_ice_shards_2.instance())
	elif tipo == 26:
		if stato==1:
			asteroido.add_child(ast_ice_3.instance())
		elif stato==2:
			asteroido.add_child(ast_ice_3_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_ice_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_ice_shards_1.instance())
		else:
			asteroido.add_child(ast_ice_shards_2.instance())
	elif tipo == 27:
		if stato==1:
			asteroido.add_child(ast_ice_4.instance())
		elif stato==2:
			asteroido.add_child(ast_ice_4_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_ice_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_ice_shards_1.instance())
		else:
			asteroido.add_child(ast_ice_shards_2.instance())
	elif tipo == 28:
		if stato==1:
			asteroido.add_child(ast_ice_5.instance())
		elif stato==2:
			asteroido.add_child(ast_ice_5_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_ice_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_ice_shards_1.instance())
		else:
			asteroido.add_child(ast_ice_shards_2.instance())


# функция анализа и добавления объекта в космос на основе пришедшей инфы с сервера
func analizo_item(item):
	if item['resurso']['objId'] == 1:#объект станция Espero
		var state = espero.instance()
		add_objekto(state,item)
		state.add_to_group('state')
	elif (item['resurso']['tipo']['objId'] == 2):# тип - корабль
		var s = sxipo.instance()
		if item['stato'] and item['stato']['objId']==5:# полностью разрушенный корабль
			s.add_child(debris.instance())
		else:
			var sh = sxipo_modulo.new()
			sh.create_sxipo(s, item)
		add_objekto(s,item)
		# добавляем маршрут движения
		if item.get('universotaskojprojektoSet') and item['universotaskojprojektoSet']:
			var projektoj = item['universotaskojprojektoSet']['edges']
			var analizo = AnalizoProjekto.new()
			analizo.analizo_projekto(projektoj,s)
		s.add_to_group('enemies')
	elif ((item['resurso']['objId'] == 7) or\
			((item['resurso']['objId'] >= 20) and\
			(item['resurso']['objId'] <= 28))) and\
			(item['koordinatoX']):# тип - астероид
#		подгружаем в зависимости от состояния модификации астероида грузим картинку
		var ast = asteroido.instance()
		krei_asteroido(ast,item['stato']['objId'],item['resurso']['objId'])
		add_objekto(ast,item)
		ast.add_to_group('asteroidoj')
	elif (item['resurso']['tipo']['objId'] == 8):# тип - Обломки космического корабля
		var d = sxipo.instance()
		d.add_child(debris.instance())
		add_objekto(d,item)
		d.add_to_group('debris')


# добавить содержание в склад
# aldoni - добавить объекты в склад
func aldoni_objekto_stokejo(uuid_objekto, uuid_modulo, stokejo):
	if !len(stokejo['objekto']['edges']):
		# если объектов нет - добавлять нечего, выходим
#		print('==объектов в складе нет uuid_objekto = ',uuid_objekto,', uuid_modulo= ',uuid_modulo)
		return
	var objekto = sercxo_objekto_uuid(uuid_objekto)
	if !objekto:
#		print('=== объект не найден === ', uuid_objekto)
		return
	var modulo = sercxado_uuid(uuid_modulo, objekto)
	if !modulo:
#		print('=== модуль не найден === ', uuid_modulo, ' = объекта = ', uuid_objekto)
		return
	if modulo: 
		Title.get_node("CanvasLayer/UI/UI/Objektoj").aldoni_elemento_stokejo(modulo.objekto, stokejo)


# изменение значений в объекте
# kien куда
# el kiu fonto - из какого источника
func sxangxi_volumeno(kien, el_kiu_fonto):
	kien['integreco'] = el_kiu_fonto['integreco']
	kien['volumenoInterna'] = el_kiu_fonto['volumenoInterna']
	kien['volumenoEkstera'] = el_kiu_fonto['volumenoEkstera']
	kien['volumenoStokado'] = el_kiu_fonto['volumenoStokado']


# изменяем вложенный объект
# direktebla - признак объект в космосе (false), или указатель на объект из списка (true) (direktebla_objekto или objektoj)
func sxangxi_enmetajxo_objekto(objekto, item, direktebla):
	#груз в связи. на 16.08.20 - руда в астероиде или на складе корабля
	# ищем, есть ли такой объект в связи
	var sxangxi_obj
#	наличие в первом уровне, а ещё есть и второй уровень - внутри грузового модуля
	var stokejo = false # объект находится на складе - добавлять не связью, а на склад

		# если находится внутри (тип = 3), то помещаем в склад соответствуюшего модуля
	if item['objekto']['ligiloLigilo']['edges'].front()['node']['tipo']['objId'] == 3:
		stokejo = true
	
	# объект в котором ресурс, передаётся во втором параметре - posedi
	# Находим объект, в котором ресурс
	if item['posedi']['uuid'] == item['mastro']['uuid']:
		# ресурс находится в гланом объекте
		if direktebla:
			sxangxi_obj = objekto
		else:
			sxangxi_obj = objekto['objekto']
	else:
		# ресурс где-то во вложенном объекте, ищём в каком
		if direktebla:
			sxangxi_obj = Title.get_node("CanvasLayer/UI/UI/Objektoj").sergxo_enmetajxo_objekto(
				objekto,item['posedi']['uuid']
			)
		else:
			sxangxi_obj = Title.get_node("CanvasLayer/UI/UI/Objektoj").sergxo_enmetajxo_objekto(
				objekto['objekto'],item['posedi']['uuid']
			)
	var trovigxo = false # наличие
#	пришла подписка на объект РУДА(objekto) на объект КОРАБЛЬ(mastro) в СКЛАД КОРАБЛЯ(posedi)
	if !stokejo:
		for obj in sxangxi_obj['ligilo']['edges']:
			#ищем ресурс в переменной
			if obj['node']['ligilo']['uuid'] == item['objekto']['uuid']:
				# вносим изменения по данному объекту
				sxangxi_volumeno(obj['node']['ligilo'], item['objekto'])
				trovigxo = true
	else:# изменяем на складе
		Title.get_node("CanvasLayer/UI/UI/Objektoj").sxangxi_elemento_stokejo_subscription(
			sxangxi_obj,
			item['objekto']
		)
		trovigxo = true
	# ищем модуль
	if !direktebla:
		for ch in objekto.get_children():
			if ch.get('uuid'):
				if (ch.uuid == item['posedi']['uuid']):
					if !stokejo:
						if trovigxo:
							# редактируем данные
							ch.sxangxi_objekto(item)
						else:
							# добавляем данные
							ch.krei_objekto(item)
					else:
							Title.get_node("CanvasLayer/UI/UI/Objektoj").sxangxi_elemento_stokejo_subscription(
								ch.objekto,
								item['objekto']
							)
					trovigxo = true
					break
	if !trovigxo:
		# если нет такого объекта, то добавляем его
		sxangxi_obj['ligilo']['edges'].push_back({
			'node': {
				'uuid': item['objekto']['ligiloLigilo']['edges'].front()['node']['uuid'],
				'tipo': item['objekto']['ligiloLigilo']['edges'].front()['node']['tipo'].duplicate(true),
				'ligilo': {
					'uuid': item['objekto']['uuid'],
					'integreco': item['objekto']['integreco'],
					'nomo': {'enhavo': item['objekto']['nomo']['enhavo']},
					'volumenoInterna': item['objekto']['volumenoInterna'],
					'volumenoEkstera': item['objekto']['volumenoEkstera'],
					'volumenoStokado': item['objekto']['volumenoStokado'],
					'resurso': {'objId': item['objekto']['resurso']['objId']}
				}
			}
		})


# изменить данные по объекту, который есть в космосе
func sxangxi_instance(objekto, item):
	# если это внутри космического объекта
	if item['mastro']:
		sxangxi_enmetajxo_objekto(objekto, item, false)
		
		if objekto == $ship: # если это управляемый корабль
			# так как управляемый объект, то изменяем в direktebla_objekto
			sxangxi_enmetajxo_objekto(Global.direktebla_objekto[Global.realeco-2], item, true)
			# перевыводим склад
			Title.get_node("CanvasLayer/UI/UI/konservejo").plenigi_formularon()
		return
	if not item['objekto']['kubo']: # объект исчез из космоса
		# удаляем из списка объектов в космосе
		var i = 0
		for obj in Global.objektoj:
			if item['objekto']['uuid'] == obj['uuid']:
				Global.objektoj.remove(i)
				break
			i += 1
		# удаляем из космоса
		objekto.queue_free()
		return
	# уточняем координаты
	# паралельно уточняем координаты в Global.objektoj
	var global_objekto = Title.get_node("CanvasLayer/UI/UI/Objektoj").sercxo_objektoj_uuid(objekto.objekto['uuid'])
	if !global_objekto:
		return
	var distance_to = objekto.translation.distance_to(Vector3(item['objekto']['koordinatoX'],
		item['objekto']['koordinatoY'], item['objekto']['koordinatoZ']))
	if distance_to > 2000:# не помогает на дистанции разница увеличивается
		objekto.translation=Vector3(item['objekto']['koordinatoX'],
			item['objekto']['koordinatoY'], item['objekto']['koordinatoZ'])
		objekto.rotation=Vector3(item['objekto']['rotaciaX'],
			item['objekto']['rotaciaY'], item['objekto']['rotaciaZ'])
		objekto.objekto['koordinatoX'] = item['objekto']['koordinatoX']
		objekto.objekto['koordinatoY'] = item['objekto']['koordinatoY']
		objekto.objekto['koordinatoZ'] = item['objekto']['koordinatoZ']
		objekto.objekto['rotaciaX'] = item['objekto']['rotaciaX']
		objekto.objekto['rotaciaY'] = item['objekto']['rotaciaY']
		objekto.objekto['rotaciaZ'] = item['objekto']['rotaciaZ']
		global_objekto['koordinatoX'] = item['objekto']['koordinatoX']
		global_objekto['koordinatoY'] = item['objekto']['koordinatoY']
		global_objekto['koordinatoZ'] = item['objekto']['koordinatoZ']
		global_objekto['rotaciaX'] = item['objekto']['rotaciaX']
		global_objekto['rotaciaY'] = item['objekto']['rotaciaY']
		global_objekto['rotaciaZ'] = item['objekto']['rotaciaZ']
	objekto.integreco = item['objekto']['integreco']
	objekto.objekto['integreco'] = item['objekto']['integreco']
	global_objekto['integreco'] = item['objekto']['integreco']
#	if item['resurso']['objId'] == 1:#объект станция Espero
#		var state = espero.instance()
#		add_objekto(state,item)
#		state.add_to_group('state')
	if (item['objekto']['resurso']['tipo']['objId'] == 2)and(item['objekto']['koordinatoX']):# тип - корабль
#		проверяем, нужно ли менять визауализацию объекта
#		if objekto == get_node('ship'): #это управляемый объект
#			if Global.logs:
#				print('попали в данные управляемого объекта')
#				# взрыв корабля
#			return
#		else:
		if objekto.objekto['stato']['objId'] == item['objekto']['stato']['objId']:
#				('оставляем визуализацию как есть')
			return
		if item['objekto']['stato']['objId']==5:# полностью разрушенный корабль
			if Global.server:
				# если это сервер - отправляем создание нового корабля и перемещение на станцию, где выдан новый корабль
				pass
			# нужна визуализация взрыва
			
			# разрушаем все части корабля
			for ch in objekto.get_children():
				if ch is CollisionShape:
					ch.queue_free()
			# добавляем осколки
			objekto.add_child(debris.instance())
		objekto.objekto['stato']['objId'] = item['objekto']['stato']['objId']
		global_objekto['stato']['objId'] = item['objekto']['stato']['objId']

	elif ((item['objekto']['resurso']['objId'] == 7) or\
			((item['objekto']['resurso']['objId'] >= 20) and\
			(item['objekto']['resurso']['objId'] <= 28))) and\
			(item['objekto']['koordinatoX']):# тип - астероид
#		проверить, нужно ли менять визауализацию объекта
		if objekto.objekto['stato']['objId'] == item['objekto']['stato']['objId']:
#			('оставляем визуализацию как есть')
			return
#		else:
#			print('меняем визуализацию объекта')
		# сохраняем предыдущее состояние
		var old_ast 
		for ch in objekto.get_children():
			if ch is Spatial:
				old_ast = ch
				break
		old_ast.queue_free()
#		подгружаем в зависимости от состояния модификации астероида грузим картинку
		krei_asteroido(objekto,item['objekto']['stato']['objId'],item['objekto']['resurso']['objId'])
		objekto.objekto['stato']['objId'] = item['objekto']['stato']['objId']


func _on_space_load_objektoj():
	""" 
	создание объектов в космосе на основании списка объектов
	"""
	# перемещаем свой корабль')
	$ship.translation=Vector3(Global.direktebla_objekto[Global.realeco-2]['koordinatoX'],
		Global.direktebla_objekto[Global.realeco-2]['koordinatoY'],
		Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'])
	if Global.direktebla_objekto[Global.realeco-2]['rotaciaX']:
		$ship.rotation=Vector3(Global.direktebla_objekto[Global.realeco-2]['rotaciaX'],
			Global.direktebla_objekto[Global.realeco-2]['rotaciaY'],
			Global.direktebla_objekto[Global.realeco-2]['rotaciaZ'])
	print('вызов distance_to из space')
	Title.get_node("CanvasLayer/UI/UI/Objektoj").distance_to($ship.translation)
	# и теперь по uuid нужно найти проект и задачу
	var projektoj = Global.direktebla_objekto[Global.realeco-2]['universotaskojprojektoSet']['edges']
	var analizo = AnalizoProjekto.new()
	analizo.analizo_projekto(projektoj,Global.fenestro_itinero)
	# создаём остальные объекты в космосе
	for item in Global.objektoj:
		analizo_item(item)
	# подписку включаем только после загрузки всех объектов
	subscribtion_kubo()


func _on_space_tree_exiting():
	"""
	разрушаем все созданные объекты в этом сцене
	"""
	for ch in get_children():
		if ch.is_in_group('create'):
			ch.free()
	Global.fenestro_kosmo = null
	
	if node_objekto:
		node_objekto.free()


# смени́ть ŝanĝi
func sxangxi_shipo(objekto):
	"""
	поменять управляемый корабль на другой
	"""
	# обновляем управляемый объект
	Title.aldoni_objekto_uzanto(objekto)
	if objekto['kubo']:
		# обновляем корабль и перемещаемся по другим координатам
		pass
	else:
		# заходим в станцию
		Global.fenestro_itinero.eniri_kosmostacio(objekto['uuid'])

