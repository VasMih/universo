extends Node


# количество (kvanto) строк для загрузки
# строка́ инф. bufrolinio.
const kvanto_bufrolinio = "10"


# запрос на получения списка категорий ресурсов
# (заказ) mendo
func mendo_resurso_kategorio(id=0, after=''):
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.push_back(id)
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'query ($realecoId:Float) '+
		' {superaUniversoResursoKategorio (realeco_Id:$realecoId,'+
		" first:"+kvanto_bufrolinio+', after: "'+after+'" ' +
		')'+
		'{ pageInfo { hasNextPage endCursor }  edges { node { uuid '+
		' nomo {enhavo} priskribo {enhavo} '+
		'ligilo{edges{node{ ligilo{'+
		' uuid nomo{enhavo} '+
		'  ligilo{edges{node{ '+
			'ligilo{ uuid nomo{enhavo} } '+
		' }}}}}}} '+
		'}}}}',
		'variables': {"realecoId":Global.realeco} }})
#	if Global.logs:
#		print('===mendo_resurso_kategorio=',query)
	Net.add_net_sendij(id, 'mendo_resurso_kategorio')
	return query


# запрос ресурсов по определённой категории
func mendo_resurso(kategorio_Id, id=0, after=''):
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.push_back(id)
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'query ($realecoId:Float, $kategorio_Id:Float) '+
		' {universoResurso (realeco_Id:$realecoId, kategorio_Id:$kategorio_Id'+
		') { edges { node { '+
		' uuid realeco{edges{node{ objId }}} '+
		' nomo { enhavo } '+
		' priskribo { enhavo } '+
		' ligilo{edges{node{ ligilo{ '+
		'  uuid nomo{enhavo} ' +
		'  ligilo{edges{node{ ligilo{ '+
			'uuid nomo{enhavo} }}}} '+
		' }}}} '+
		' } } } } ',
		'variables': {"realecoId":Global.realeco, "kategorio_Id":kategorio_Id} }})
#	if Global.logs:
#		print('===mendo_resurso=',query)
	Net.add_net_sendij(id, 'mendo_resurso')
	return query

